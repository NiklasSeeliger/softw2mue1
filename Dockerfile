FROM maven:3.8.6-openjdk-18
EXPOSE 8080
COPY target/*.jar /
CMD java -jar *.jar
