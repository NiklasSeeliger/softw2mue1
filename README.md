# Über dieses Repo

[![pipeline status](https://gitlab.com/NiklasSeeliger/softw2mue1/badges/main/pipeline.svg)](https://gitlab.com/NiklasSeeliger/softw2mue1/-/commits/main)
[![Coverage](https://gitlab.com/niklasseeliger/softw2mue1/badges/main/coverage.svg)](https://gitlab.com/niklasseeliger/sofw2mue1/)
[![Latest Release](https://gitlab.com/NiklasSeeliger/softw2mue1/-/badges/release.svg)](https://gitlab.com/NiklasSeeliger/softw2mue1/-/releases)


Dies ist meine Lösung zu den SOFTW2 modulbezogenen Übungen 1, 2 & 3

Es handelt sich um eine Datenbankanwendung, die Filme mit Namen und ID speichert und über eine REST-API bereitstellt. 

Zum Interagieren mit der API zu Testzwecken ist Postman zu empfehlen. 

Benutzte Technologien sind:
- Maven
- Spring-boot
- JUnit
- JPA
- Hibernate
- Pitest
- Jacoco
- Docker
- Postgres

## 🔧 Prärequisiten

Zum Entwickeln muss folgende Software bereitstehen:

- Git
- JDK
- Maven (mit `mvnw` kann auch der Maven-Wrapper benutzt werden)

Zum Ausführen der Applikation muss folgende Software bereitstehen:

- Docker
- Docker-compose

## 🚀 Ausführen

Zum Ausführen muss lediglich ausgeführt werden:
```shell
docker-compose up -d
```

Es wird die neueste Version der Gitlab-Registry gestartet. Oder wenn ein Image local gebuilded wurde (siehe Abschnitt Docker-Image), wird diese Version ausgeführt. Um die lokale App-Version mit der aus der Registry zu ersetzen muss `docker-compose pull` ausgeführt werden.

Zum Stoppen der Anwendung:
```shell
docker-compose down
```

Datenbankdaten werden hierdurch nicht gelöscht. Sollten die gespeicherten Daten ebenfalls gelöscht werden, muss ausgeführt werden:
```shell
docker-compose down -v
```

## 🧪 Testen

Um Unit-, Mutations-, und Coverage-Tests auszuführen muss im Wurzelverzeichnis ausgeführt werden:

```shell
mvn clean verify
```

Dies ist auch der Befehl, der in der Test-Phase von der CI/CD Pipeline ausgeführt wird. So kann also getestet werden, ob die Tests der Pipeline im aktuellen Stand fehlerfrei funktionieren. 

Um die Integrationstests auszuführen muss zunächst die Postgres-DB gestartet werden:

```shell
docker run -p 5432:5432 -e POSTGRES_USER=username -e POSTGRES_PASSWORD=password -e POSTGRES_DB=application_db postgres
```

Der Datenbank kann auch ein sicherer Usename und Passwort vergeben werden. Die Umgebungsvariablen `DB_USER` und `DB_PASSWORD` sollten dann allerdings auf richtigen Werte gesetzt werden, um die Applikation die Richtigen Credentials mitzuteilen. Docker Compose wird diese Variablen ebenfalls benutzen, um die Datenbank besser zu schützen. 

Im Wurzelverzeichniss können dann die Integrationstests ausgeführt werden:

```shell
mvn test -Pint-tests
```


## 🏗️ Debuggen

Zum Debuggen muss zunächst die Postgres-DB gestartet werden (Befehl hierfür steht oben).

Dann kann die Applikation gestartet werden mit dem Befehl:
```shell
mvn spring-boot:run
```

Die Anwendung kann mit Strg+C und danach mit y zum Bestätigen beendet werden. Zu Bedenken ist allerdings, dass die Datenbank noch läuft und die Daten persistent sind (bis auch die Datenbank gestoppt wird).


## 📦️ Docker-Image

Das Applikations-Docker-Image muss im Regelfall nicht manuell erstellt werden, da es von der CI/CD Pipeline automatisch erstellt und in die Registry gepusht wird. 

Sollte das Docker-Image dennoch manuell erstellt werden, muss folgendes ausgeführt werden:

```shell
mvn clean package docker:build
```

Dann zum pushen des Images zur Gitlab-Container-Registry (eventuell muss man sich voher mit `docker login registry.gitlab.com` einloggen):

```shell
mvn docker:push
```

Zum starten des Docker-Images sollte ausgeführt werden:

```shell
docker run -p 8080:8080 -e DB_HOST={IP}
```

Wo {IP} mit der IP-Adresse der Postgres-DB ersetzt werden muss.
