package softw2.MovieDB;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class MovieDbIntegrationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /*
     * Test cases für Blatt 1
     */

    @Test
    void testCase1() {
        // get request returns empty list
        assertThat(restTemplate.getForObject("http://localhost:" + port + "/movies", String.class)).isEqualTo("[]");
    }

    @Test
    void testCase2() {
        // post request returns code 201
        String requestBody = "{\"name\":\"Top Gun\"}";

        final HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                String.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertThat(response.getBody()).isNullOrEmpty();
    }

    @Test
    void testCase3() {
        String requestBody = "{\"name\":\"Who am I\"}";

        final HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

        // insert Who am I
        restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                String.class);

        // now check contents and assert that the inserted movie is contained
        assertEquals("[{\"id\":0,\"name\":\"Who am I\"}]", restTemplate.getForObject("http://localhost:" + port + "/movies", String.class));
    }

    @Test
    void testCase4() {
        // insert King Kong
        {
            String requestBody = "{\"name\":\"King Kong\"}";

            final HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");

            final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

            restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                    String.class);
        }
        

        // insert 300
        {
            String requestBody = "{\"name\":\"300\"}";

            final HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
    
            final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);
    
            restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                    String.class);
        }
        
        // now check both movies are contained
        assertEquals("[{\"id\":0,\"name\":\"King Kong\"},{\"id\":1,\"name\":\"300\"}]", restTemplate.getForObject("http://localhost:" + port + "/movies", String.class));
    }

    @Test
    void testCase5() {
        String requestBody = "{\"namos\":\"Die Unglaublichen\"}";

        final HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                String.class);

        // now check that the request was denied
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertThat(response.getBody()).isNullOrEmpty();
    }

    @Test
    void testCase6() {
        // send faulty request
        {
            String requestBody = "{\"namos\":\"Die Unglaublichen\"}";

            final HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");

            final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

            ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                    String.class);
        }
        // send correct request
        {
            String requestBody = "{\"name\":\"San Andreas\"}";

            final HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");

            final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

            ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                    String.class);
        }

        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/movies", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("[{\"id\":0,\"name\":\"San Andreas\"}]", response.getBody());
    }

    /*
     * Testcases für Blatt 2
     */

    @Test
    void testCase7() {
        // get request returns nothing and 404
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/movies/1", String.class);

        assertThat(response.getBody()).isNullOrEmpty();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void testCase8() {
        //inserting movie
        {
            String requestBody = "{\"name\":\"GhostBusters\"}";

            final HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");

            final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

            ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                    String.class);
        }

        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/movies/0", String.class);

        // searching for movie by id
        assertThat(response.getBody()).isEqualTo("{\"id\":0,\"name\":\"GhostBusters\"}");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void testCase9() {
        // search for name returns empty list
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/movies/Scream", String.class);

        assertThat(response.getBody()).isEqualTo("[]");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void testCase10() {
        //inserting movie
        {
            String requestBody = "{\"name\":\"Prey\"}";

            final HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");

            final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

            ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                    String.class);
        }

        // search for name returns inserted movie
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/movies/Prey", String.class);

        assertThat(response.getBody()).isEqualTo("[{\"id\":0,\"name\":\"Prey\"}]");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void testCase11() {
        // inserting multiple movies by the same name
        for (int i = 0; i < 2; i++){
            String requestBody = "{\"name\":\"Prey\"}";

            final HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");

            final HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

            ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port + "/movies", entity,
                    String.class);
        }

        // searching for the name ane expecting both movies
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/movies/Prey", String.class);

        assertThat(response.getBody()).isEqualTo("[{\"id\":0,\"name\":\"Prey\"},{\"id\":1,\"name\":\"Prey\"}]");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
