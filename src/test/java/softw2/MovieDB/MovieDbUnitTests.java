package softw2.MovieDB;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class MovieDbUnitTests {

	@Autowired
	private MovieRepository movieRepository;

	private MovieDbController controller;

	@BeforeEach
	void setUp() {
		controller = new MovieDbController(movieRepository);
	}

	/*
	 * Blatt 1 Unit tests
	 */
	@Test
	@Order(1)
	void contextLoads() {
		assertThat(movieRepository).isNotNull();
		assertThat(controller).isNotNull();
	}

	@Test
	void testMovieNameGetterAndSetter() {
		Movie movie = new Movie(0,"");
		movie.setName("Top Gun");
		assertThat(movie.getName()).isEqualTo("Top Gun");
	}

	@Test
	void testMovieIdGetterAndSetter() {
		Movie movie = new Movie(0,"");
		movie.setId(1);
		assertThat(movie.getId()).isEqualTo(1);
	}

	@Test
	void testStartWithEmptyMovieList() {
		assertThat(controller.getMovies()).isEmpty();
	}
	
	@Test
	void testInsertMovie() {
		// after a movie is inserted, getting the list of movies should return the movie
		ResponseEntity<Movie> response = controller.addMovie(new Movie(-1, "Toy Story"));
		assertThat(controller.getMovies().get(0).getName()).isEqualTo("Toy Story");
		assertThat(controller.getMovies().get(0).getId()).isEqualTo(0);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
	}

	@Test
	void testNoNameSpecified() {
		ResponseEntity<Movie> response = controller.addMovie(new Movie(-1, ""));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

	@Test 
	void testNullNameSpecified() {
		ResponseEntity<Movie> response = controller.addMovie(new Movie(-1, null));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

	@Test
	void testNoMovieSpecified() {
		ResponseEntity<Movie> response = controller.addMovie(null);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

	@Test
	void testIdCounter() {
		controller.addMovie(new Movie(-1, "Toy Story"));
		controller.addMovie(new Movie(-1, "Who am I"));
		assertThat(controller.getMovies().get(0).getId()).isEqualTo(0);
		assertThat(controller.getMovies().get(1).getId()).isEqualTo(1);
	}

	@Test
	void testAllMoviesAreInserted() {
		controller.addMovie(new Movie(-1, "Toy Story"));
		controller.addMovie(new Movie(-1, "Who am I"));
		controller.addMovie(new Movie(-1, "Top Gun"));
		controller.addMovie(new Movie(-1, "Toy Story"));
		controller.addMovie(new Movie(-1, "Who am I"));
		controller.addMovie(new Movie(-1, "Top Gun"));
		controller.addMovie(new Movie(-1, "Toy Story"));
		controller.addMovie(new Movie(-1, "Who am I"));
		controller.addMovie(new Movie(-1, "Top Gun"));
		assertThat(controller.getMovies().size()).isEqualTo(9);
	}

	/*
	 * Blatt 2 Unit tests
	 */

	@Test
	void testMovieById() {
		controller.addMovie(new Movie(-1, "Toy Story"));
		assertThat(controller.getMovieById(0).getBody().getName()).isEqualTo("Toy Story");
		assertThat(controller.getMovieById(0).getBody().getId()).isEqualTo(0);
	}

	@Test
	void testSearchForMovie() {
		controller.addMovie(new Movie(-1, "Toy Story"));
		assertThat(((ResponseEntity<List<Movie>>) controller.searchForMovie("Toy Story")).getBody().get(0).getName()).isEqualTo("Toy Story");
		assertThat(((ResponseEntity<List<Movie>>) controller.searchForMovie("Toy Story")).getBody().get(0).getId()).isEqualTo(0);
		assertThat(((ResponseEntity<List<Movie>>) controller.searchForMovie("Toy Story")).getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	void testSearchForMovieById() {
		controller.addMovie(new Movie(-1, "Toy Story"));
		assertThat(((ResponseEntity<Movie>) controller.searchForMovie("0")).getBody().getName()).isEqualTo("Toy Story");
		assertThat(((ResponseEntity<Movie>) controller.searchForMovie("0")).getBody().getId()).isEqualTo(0);
		assertThat(((ResponseEntity<Movie>) controller.searchForMovie("0")).getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	void testSearchForMovieByIdNotFound() {
		assertThat(((ResponseEntity<Movie>) controller.searchForMovie("0")).getBody()).isNull();
		assertThat(((ResponseEntity<Movie>) controller.searchForMovie("0")).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}
}
