package softw2.MovieDB;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.bind.annotation.*;

@RestController
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class MovieDbController {

    @Autowired
    private MovieRepository movieRepository;

    public MovieDbController(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @GetMapping(value = "/movies", produces = "application/json")
    public List<Movie> getMovies() {
        return movieRepository.findAll();
    }

    @PostMapping(value = "/movies", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        if (movie == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (movie.getName() == null || movie.getName().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        movieRepository.save(movie);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/movies/{searchTerm}", produces = "application/json")
    public ResponseEntity searchForMovie(@PathVariable String searchTerm) {
        if (searchTerm.matches("\\d+"))
            return getMovieById(Integer.parseInt(searchTerm));
        else
            return getMovieByName(searchTerm);
    }

    public ResponseEntity<Movie> getMovieById(@PathVariable int id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(movie.get(), HttpStatus.OK);
    }

    public ResponseEntity<List<Movie>> getMovieByName(@PathVariable String name) {
        return new ResponseEntity<>(movieRepository.findByName(name), HttpStatus.OK);
    }
}
